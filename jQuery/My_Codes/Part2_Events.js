$('h1').click(function(){
  console.log("There was a click!");
})

$('li').click(function(){
  $(this).text('List changed')
})

$('input').eq(0).keypress(function(event){
  if(event.which === 13){
    $('h3').toggleClass('turnBlue')
  }
})

$('h1').on('dblclick',function(){
  $(this).toggleClass('turnRed')
})

$('input').eq(1).on('click',function(){
  $('.container').fadeOut(3000)
})

$('input').eq(0).on('click',function(){
  $('.container').slideUp(3000)
})
