var cels = document.querySelectorAll('td')

var reset = document.querySelector('button')

function clearBoard(){
  for (var i = 0; i < cels.length; i++) {
    cels[i].textContent = ''
  }
}

function cycleText(){
  if (this.textContent == "X") {
    this.textContent = "O";
  } else if (this.textContent == "O") {
    this.textContent = "";
  } else {
    this.textContent = "X";
  }
}

for (var i = 0; i < cels.length; i++) {
  cels[i].addEventListener('click', cycleText)
}

reset.addEventListener('click', clearBoard)
